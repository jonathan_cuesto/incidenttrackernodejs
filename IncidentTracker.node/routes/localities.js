﻿'use strict';
var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema
var localitiesSchema = new mongoose.Schema({
    name : String
});

//return
module.exports = restful.model('localities', localitiesSchema);