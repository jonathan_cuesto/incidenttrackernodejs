﻿'use strict';
var express = require('express');
var router = express.Router();

//models
var localities = require('./localities');
var incidents = require('./incidents');

//routes
localities.methods(['get']);
incidents.methods(['get', 'post']);
localities.register(router, '/localities');
incidents.register(router, '/incidents');


/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Incident Tracker' });
});

module.exports = router;