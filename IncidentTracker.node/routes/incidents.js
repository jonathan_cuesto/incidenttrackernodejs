﻿'use strict';
var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema
var incidentsSchema = new mongoose.Schema({
    name: String,
    kind: String,
    locationId: String,
    happenedAt: Date,
    isArchived: Boolean
});

//return
module.exports = restful.model('incidents', incidentsSchema);